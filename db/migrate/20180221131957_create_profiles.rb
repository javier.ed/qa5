class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.string :gender
      t.string :lang
      t.string :country_code
      t.string :avatar
      t.references :user, index: {:unique=>true}, foreign_key: true

      t.timestamps
    end
  end
end
