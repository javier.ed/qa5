class RemoveTitleInPanels < ActiveRecord::Migration[5.1]
  def change
    remove_column :panels, :title
  end
end
