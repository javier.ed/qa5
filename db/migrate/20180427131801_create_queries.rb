class CreateQueries < ActiveRecord::Migration[5.1]
  def change
    create_table :queries do |t|
      t.references :panel, foreign_key: true
      t.string :query

      t.timestamps
    end
  end
end
