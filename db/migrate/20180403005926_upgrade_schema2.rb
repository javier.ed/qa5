class UpgradeSchema2 < ActiveRecord::Migration[5.1]
  def change
    rename_column :panel_options, :template_panel_id, :panel_id
    rename_index :panel_options, :index_panel_options_on_template_panel_id, :index_panel_options_on_panel_id
    rename_column :user_panels, :template_panel_id, :panel_id
    rename_index :user_panels, :index_user_panels_on_template_panel_id, :index_user_panels_on_panel_id
  end
end
