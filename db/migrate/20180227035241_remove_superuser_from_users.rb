class RemoveSuperuserFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :superuser
  end
end
