class RenameColumnType < ActiveRecord::Migration[5.1]
  def change
    rename_column :panels, :type, :dataset_type
    rename_column :panel_data, :type, :data_type
  end
end
