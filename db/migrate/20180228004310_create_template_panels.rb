class CreateTemplatePanels < ActiveRecord::Migration[5.1]
  def change
    create_table :template_panels do |t|
      t.string :location
      t.string :label
      t.string :display_type

      t.timestamps
    end

    create_table :template_panel_options do |t|
      t.references :template_panel, foreign_key: true
      t.string :label
      t.string :display_type

      t.timestamps
    end

    add_reference :panels, :template_panel, foreign_key: true
    remove_column :panels, :location
    remove_column :panels, :dataset_type
    remove_column :panels, :dataset

    rename_table :panel_data, :panel_options
    add_reference :panel_options, :template_panel_option, foreign_key: true
    remove_column :panel_options, :data_type
    remove_column :panel_options, :data
  end
end
