class CreateCallFileStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :call_file_statuses do |t|
      t.references :call, foreign_key: true
      t.string :status

      t.timestamps
    end
  end
end
