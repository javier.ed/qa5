class CreatePanelData < ActiveRecord::Migration[5.1]
  def change
    create_table :panel_data do |t|
      t.references :panel, foreign_key: true
      t.string :type
      t.string :data

      t.timestamps
    end
  end
end
