class DropTablePanelOptions < ActiveRecord::Migration[5.1]
  def change
    drop_table :panel_options
  end
end
