class RenameEmotionInCallSegmentEmotions < ActiveRecord::Migration[5.1]
  def change
    rename_column :call_segment_emotions, :emotion, :name
  end
end
