class RenameQueriesToPanelQueries < ActiveRecord::Migration[5.2]
  def change
    rename_table :queries, :panel_queries
    add_column :panel_queries, :name, :string
  end
end
