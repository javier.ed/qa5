class AddColumnCsvFileToCalls < ActiveRecord::Migration[5.2]
  def change
    add_column :calls, :csv_file, :string
    add_column :call_segments, :uncertain, :integer
    add_column :call_segments, :excited, :integer
    add_column :call_segments, :embarrassment, :integer
    add_column :call_segments, :atmosphere, :integer
    add_column :call_segments, :lionet_analysis, :string
    add_column :call_segments, :online_flag, :string
    add_column :call_segments, :comment, :string
    add_column :call_segments, :lionet_info, :string
    rename_column :call_segments, :lva_glb_stress, :lva_global_stress
  end
end
