class CreateCalls < ActiveRecord::Migration[5.1]
  def change
    create_table :calls do |t|
      t.datetime :made_at
      t.float :duration
      t.references :agent, foreign_key: true
      t.string :tag_name
      t.integer :tag_value
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
