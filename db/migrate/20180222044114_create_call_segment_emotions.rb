class CreateCallSegmentEmotions < ActiveRecord::Migration[5.1]
  def change
    create_table :call_segment_emotions do |t|
      t.references :call_segment, foreign_key: true
      t.string :emotion
      t.integer :value

      t.timestamps
    end
  end
end
