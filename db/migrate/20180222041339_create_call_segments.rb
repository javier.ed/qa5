class CreateCallSegments < ActiveRecord::Migration[5.1]
  def change
    create_table :call_segments do |t|
      t.references :call, foreign_key: true
      t.integer :number
      t.float :start
      t.float :end
      t.integer :cnl
      t.integer :voice_energy
      t.string :lionet_analysis
      t.string :online_flag
      t.string :comment
      t.string :lionet_info

      t.timestamps
    end
  end
end
