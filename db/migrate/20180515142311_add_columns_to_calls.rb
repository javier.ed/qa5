class AddColumnsToCalls < ActiveRecord::Migration[5.2]
  def change
    add_column :calls, :tag, :string
    add_column :calls, :highest_quality_priority, :integer
    add_column :calls, :final_quality_priority, :integer
    add_column :calls, :agent_priority_rank, :integer
    add_column :calls, :dissatisfaction_score, :float
    add_column :calls, :avg_voice_energy, :integer
    add_column :calls, :anger_percentage, :float
    add_column :calls, :stress_percentage, :float
    add_column :calls, :upset_percentage, :float
    add_column :calls, :content_percentage, :float
    add_column :calls, :avg_emotional_energy, :float
    add_column :calls, :energy_level, :integer
    add_column :calls, :arousal_level, :integer
    add_column :calls, :emotion_level, :integer
    add_column :calls, :uneasy_level, :integer
    add_column :calls, :stress_level, :integer
    add_column :calls, :thinking_level, :integer
    add_column :calls, :confidence, :integer
    add_column :calls, :concentration, :integer
    add_column :calls, :anticipation, :integer
    add_column :calls, :csc_score, :integer
    add_column :calls, :fi10_score, :integer
    add_column :calls, :file_format, :string
  end
end
