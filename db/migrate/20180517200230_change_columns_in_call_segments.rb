class ChangeColumnsInCallSegments < ActiveRecord::Migration[5.2]
  def change
    add_column :call_segments, :global_segment, :string
    add_column :call_segments, :topic, :string
    add_column :call_segments, :channel, :string
    add_column :call_segments, :online_lva, :string
    add_column :call_segments, :offline_lva, :string
    add_column :call_segments, :risk1, :integer
    add_column :call_segments, :risk2, :integer
    add_column :call_segments, :riskoz, :integer
    add_column :call_segments, :oz1oz2oz3, :string
    add_column :call_segments, :cognitive_level, :integer
    add_column :call_segments, :emotional_level, :integer
    add_column :call_segments, :anticipation, :integer
    add_column :call_segments, :emotional_balance, :integer
    add_column :call_segments, :extreme_emotion, :integer
    add_column :call_segments, :cognition_high_low_balance, :integer
    add_column :call_segments, :lva_risk_stress, :integer
    add_column :call_segments, :lva_glb_stress, :integer
    add_column :call_segments, :lva_emotional_stress, :integer
    add_column :call_segments, :lva_cognitive_stress, :integer
    add_column :call_segments, :lva_energetic_stress, :integer
    remove_column :call_segments, :cnl
    remove_column :call_segments, :lionet_analysis
    remove_column :call_segments, :online_flag
    remove_column :call_segments, :comment
    remove_column :call_segments, :lionet_info
    change_column :call_segments, :energy, :integer
    change_column :call_segments, :content, :integer
    change_column :call_segments, :upset, :integer
    change_column :call_segments, :angry, :integer
    change_column :call_segments, :stress, :integer
    remove_column :call_segments, :uncertainty
    remove_column :call_segments, :excitement
    change_column :call_segments, :concentration_level, :integer
    change_column :call_segments, :emocog_ratio, :integer
    change_column :call_segments, :hesitation, :integer
    change_column :call_segments, :brain_power, :integer
    remove_column :call_segments, :embarrassment
    change_column :call_segments, :intensive_thinking, :integer
    change_column :call_segments, :imagination_activity, :integer
    remove_column :call_segments, :extreme_state
    change_column :call_segments, :saf, :integer
    remove_column :call_segments, :atmosphere
  end
end
