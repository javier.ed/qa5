class AddDatasetToPanels < ActiveRecord::Migration[5.1]
  def change
    add_column :panels, :dataset, :string
  end
end
