class AddColorAndSelectAndWhereToTemplatePanelOptions < ActiveRecord::Migration[5.1]
  def change
    add_column :template_panel_options, :color, :string
    add_column :template_panel_options, :select, :string
    add_column :template_panel_options, :where, :string
  end
end
