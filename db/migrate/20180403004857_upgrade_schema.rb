class UpgradeSchema < ActiveRecord::Migration[5.1]
  def change
    drop_table :call_segment_emotions
    rename_table :panels, :user_panels
    add_reference :user_panels, :user, foreign_key: true
    rename_table :template_panels, :panels
    drop_table :panel_options
    rename_table :template_panel_options, :panel_options
  end
end
