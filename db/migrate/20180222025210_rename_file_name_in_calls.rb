class RenameFileNameInCalls < ActiveRecord::Migration[5.1]
  def change
    rename_column :calls, :file_name, :file
  end
end
