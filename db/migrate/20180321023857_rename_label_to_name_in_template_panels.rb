class RenameLabelToNameInTemplatePanels < ActiveRecord::Migration[5.1]
  def change
    rename_column :template_panels, :label, :name
    add_column :template_panels, :query, :text
  end
end
