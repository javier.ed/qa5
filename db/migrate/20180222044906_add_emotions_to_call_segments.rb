class AddEmotionsToCallSegments < ActiveRecord::Migration[5.1]
  def change
    add_column :call_segments, :energy, :string
    add_column :call_segments, :content, :string
    add_column :call_segments, :upset, :string
    add_column :call_segments, :angry, :string
    add_column :call_segments, :stress, :string
    add_column :call_segments, :uncertainty, :string
    add_column :call_segments, :excitement, :string
    add_column :call_segments, :concentration_level, :string
    add_column :call_segments, :emocog_ratio, :string
    add_column :call_segments, :hesitation, :string
    add_column :call_segments, :brain_power, :string
    add_column :call_segments, :embarrassment, :string
    add_column :call_segments, :intensive_thinking, :string
    add_column :call_segments, :imagination_activity, :string
    add_column :call_segments, :extreme_state, :string
    add_column :call_segments, :saf, :string
    add_column :call_segments, :atmosphere, :string
  end
end
