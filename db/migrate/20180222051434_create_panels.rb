class CreatePanels < ActiveRecord::Migration[5.1]
  def change
    create_table :panels do |t|
      t.string :location
      t.string :title
      t.string :type
      t.integer :position

      t.timestamps
    end
  end
end
