class CreateEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :emails do |t|
      t.string :email
      t.boolean :primary
      t.datetime :verified_at
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :emails, :email, unique: true
  end
end
