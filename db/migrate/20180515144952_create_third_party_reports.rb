class CreateThirdPartyReports < ActiveRecord::Migration[5.2]
  def change
    create_table :third_party_reports do |t|
      t.string :file
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_reference :calls, :third_party_report, foreign_key: true
  end
end
