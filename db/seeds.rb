# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create superuser
if User.find_by(role: 'superuser').nil?
  puts "Whould you like to create a superuser now? (Y/n) #{ENV['CREATE_SUPERUSER']}"
  response = ENV['CREATE_SUPERUSER'] || STDIN.gets.chomp
  if !%w[n N].include? response
    user = User.new
    user.profile = Profile.new
    puts "Username: #{ENV['SUPERUSER_USERNAME']}"
    user.username = ENV['SUPERUSER_USERNAME'] || STDIN.gets.chomp
    puts "Email: #{ENV['SUPERUSER_EMAIL']}"
    user.emails.build(email: ENV['SUPERUSER_EMAIL'] || STDIN.gets.chomp)
    puts "Password: #{ENV['SUPERUSER_PASSWORD']}"
    user.password = ENV['SUPERUSER_PASSWORD'] || STDIN.gets.chomp
    puts "Password confirmation: #{ENV['SUPERUSER_PASSWORD']}"
    user.password_confirmation = ENV['SUPERUSER_PASSWORD'] || STDIN.gets.chomp
    puts "First name: #{ENV['SUPERUSER_FIRSTNAME']}"
    user.profile.first_name = ENV['SUPERUSER_FIRSTNAME'] || STDIN.gets.chomp
    puts "Last name: #{ENV['SUPERUSER_LASTNAME']}"
    user.profile.last_name = ENV['SUPERUSER_LASTNAME'] || STDIN.gets.chomp
    puts "Birthdate (YYYY-MM-DD): #{ENV['SUPERUSER_BIRTHDATE']}"
    user.profile.birthdate = ENV['SUPERUSER_BIRTHDATE'] || STDIN.gets.chomp
    puts "Gender (male/female): #{ENV['SUPERUSER_GENDER']}"
    user.profile.gender = ENV['SUPERUSER_GENDER'] || STDIN.gets.chomp
    puts "Country (example: VE): #{ENV['SUPERUSER_COUNTRY']}"
    user.profile.country = ENV['SUPERUSER_COUNTRY'] || STDIN.gets.chomp
    user.role = 'superuser'
    unless user.save
      puts 'Errors where found!'
      user.errors.each do |method|
        puts "#{method}: #{user.errors[method]}"
      end
      raise 'Failed to create superuser.'
    end
    puts 'Superuser created.'
  else
    puts 'Continue without creating a superuser.'
  end
else
  puts 'Superuser already exists.'
end
