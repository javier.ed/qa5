# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_24_140219) do

  create_table "call_file_statuses", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "call_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["call_id"], name: "index_call_file_statuses_on_call_id"
  end

  create_table "call_segments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "call_id"
    t.integer "number"
    t.float "start"
    t.float "end"
    t.integer "voice_energy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "energy"
    t.integer "content"
    t.integer "upset"
    t.integer "angry"
    t.integer "stress"
    t.integer "concentration_level"
    t.integer "emocog_ratio"
    t.integer "hesitation"
    t.integer "brain_power"
    t.integer "intensive_thinking"
    t.integer "imagination_activity"
    t.integer "saf"
    t.string "global_segment"
    t.string "topic"
    t.string "channel"
    t.string "online_lva"
    t.string "offline_lva"
    t.integer "risk1"
    t.integer "risk2"
    t.integer "riskoz"
    t.string "oz1oz2oz3"
    t.integer "cognitive_level"
    t.integer "emotional_level"
    t.integer "anticipation"
    t.integer "emotional_balance"
    t.integer "extreme_emotion"
    t.integer "cognition_high_low_balance"
    t.integer "lva_risk_stress"
    t.integer "lva_global_stress"
    t.integer "lva_emotional_stress"
    t.integer "lva_cognitive_stress"
    t.integer "lva_energetic_stress"
    t.integer "uncertain"
    t.integer "excited"
    t.integer "embarrassment"
    t.integer "atmosphere"
    t.string "lionet_analysis"
    t.string "online_flag"
    t.string "comment"
    t.string "lionet_info"
    t.index ["call_id"], name: "index_call_segments_on_call_id"
  end

  create_table "calls", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "made_at"
    t.float "duration"
    t.integer "agent_id"
    t.string "tag_name"
    t.integer "tag_value"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file"
    t.string "tag"
    t.integer "highest_quality_priority"
    t.integer "final_quality_priority"
    t.integer "agent_priority_rank"
    t.float "dissatisfaction_score"
    t.integer "avg_voice_energy"
    t.float "anger_percentage"
    t.float "stress_percentage"
    t.float "upset_percentage"
    t.float "content_percentage"
    t.float "avg_emotional_energy"
    t.integer "energy_level"
    t.integer "arousal_level"
    t.integer "emotion_level"
    t.integer "uneasy_level"
    t.integer "stress_level"
    t.integer "thinking_level"
    t.integer "confidence"
    t.integer "concentration"
    t.integer "anticipation"
    t.integer "csc_score"
    t.integer "fi10_score"
    t.string "file_format"
    t.integer "third_party_report_id"
    t.string "name"
    t.string "csv_file"
    t.index ["agent_id"], name: "index_calls_on_agent_id"
    t.index ["third_party_report_id"], name: "index_calls_on_third_party_report_id"
    t.index ["user_id"], name: "index_calls_on_user_id"
  end

  create_table "emails", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email"
    t.boolean "primary"
    t.datetime "verified_at"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_emails_on_email", unique: true
    t.index ["user_id"], name: "index_emails_on_user_id"
  end

  create_table "panel_queries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "panel_id"
    t.string "query"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["panel_id"], name: "index_panel_queries_on_panel_id"
  end

  create_table "panels", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "location"
    t.string "name"
    t.string "display_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "query"
  end

  create_table "profiles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birthdate"
    t.string "gender"
    t.string "lang"
    t.string "country_code"
    t.string "avatar"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_profiles_on_user_id", unique: true
  end

  create_table "third_party_reports", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "file"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_third_party_reports_on_user_id"
  end

  create_table "user_panels", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "panel_id"
    t.integer "user_id"
    t.index ["panel_id"], name: "index_user_panels_on_panel_id"
    t.index ["user_id"], name: "index_user_panels_on_user_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role", default: "user"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
