import CallsIndex from '../pages/calls/index'
import CallsNew from '../pages/calls/new'
import CallsUploadReports from '../pages/calls/upload_reports'

export default { path: '/calls', component: CallsIndex,
                 children: [{ path: 'new', component: CallsNew },
                            { path: 'upload_reports', component: CallsUploadReports }] }
