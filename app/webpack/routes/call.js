import CallsShow from '../pages/calls/show'
import CallsEdit from '../pages/calls/edit'
import CallsShare from '../pages/calls/share'

export default { path: '/calls/:id', component: CallsShow,
                 children: [{ path: 'edit', component: CallsEdit },
                            { path: 'share', component: CallsShare }] }
