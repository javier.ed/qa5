import Index from '../pages/index'
import Share from '../pages/share'

export default { path: '/', component: Index, children: [{ path: 'share', component: Share }] }