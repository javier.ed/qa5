import AgentsIndex from '../pages/agents/index'
import AgentsShow from '../pages/agents/show'

export default { path: '/agents', component: AgentsIndex,
                 children: [{ path: ':id', component: AgentsShow }] }
