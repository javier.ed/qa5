import SettingsIndex from '../pages/settings/index'
import SettingsProfile from '../pages/settings/profile'
import SettingsEmails from '../pages/settings/emails'
import SettingsPassword from '../pages/settings/password'
import SettingsAccount from '../pages/settings/account'

export default { path: '/settings', component: SettingsIndex,
                 children: [{ path: 'profile', component: SettingsProfile },
                            { path: 'emails', component: SettingsEmails },
                            { path: 'password', component: SettingsPassword },
                            { path: 'account', component: SettingsAccount }] }
