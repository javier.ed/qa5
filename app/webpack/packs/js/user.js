/* eslint no-console:0 */
import { Vue, VueRouter } from '../../base'
import apolloProvider from '../../plugins/apollo'

import HomeRoutes from '../../routes/home'
import CallsRoutes from '../../routes/calls'
import CallRoutes from '../../routes/call'
import AgentsRoutes from '../../routes/agents'
import SettingsRoutes from '../../routes/settings'
import SignOut from '../../pages/sign_out'
import Terms from '../../pages/terms'
import Privacy from '../../pages/privacy'
import Contact from '../../pages/contact'

const router = new VueRouter({
  mode: 'history',
  routes: [
    HomeRoutes,
    CallsRoutes,
    CallRoutes,
    AgentsRoutes,
    SettingsRoutes,
    { path: '/sign_out', component: SignOut },
    { path: '/terms', component: Terms },
    { path: '/privacy', component: Privacy },
    { path: '/contact', component: Contact }
  ]
})

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#app',
    router,
    provide: apolloProvider.provide(),
    methods: {
      activateItem(ref, index) {
        this.$refs[ref].activeIndex = index
      }
    }
  })
})
