/* eslint no-console:0 */
import { Vue, VueRouter } from '../../base'
import apolloProvider from '../../plugins/apollo'

import HomeRoutes from '../../routes/home'
import CallsRoutes from '../../routes/calls'
import CallRoutes from '../../routes/call'
import AgentsRoutes from '../../routes/agents'
import SettingsRoutes from '../../routes/settings'
import AdminIndex from '../../pages/admin/index'
import AdminPanelsIndex from '../../pages/admin/panels/index'
import AdminPanelsNew from '../../pages/admin/panels/new'
import AdminPanelsEdit from '../../pages/admin/panels/edit'
import AdminAlertsIndex from '../../pages/admin/alerts/index'
import AdminLabelsIndex from '../../pages/admin/labels/index'
import AdminUsersIndex from '../../pages/admin/users/index'
import AdminUsersNew from '../../pages/admin/users/new'
import AdminUsersEdit from '../../pages/admin/users/edit'
import Terms from '../../pages/terms'
import Privacy from '../../pages/privacy'
import Contact from '../../pages/contact'

const router = new VueRouter({
  mode: 'history',
  routes: [
    HomeRoutes,
    CallsRoutes,
    CallRoutes,
    AgentsRoutes,
    SettingsRoutes,
    { path: '/admin', component: AdminIndex,
      children: [{ path: 'panels', component: AdminPanelsIndex,
                   children: [{ path: 'new', component: AdminPanelsNew },
                              { path: ':id/edit', component: AdminPanelsEdit }] },
                 { path: 'alerts', component: AdminAlertsIndex },
                 { path: 'labels', component: AdminLabelsIndex },
                 { path: 'users', component: AdminUsersIndex,
                   children: [{ path: 'new', component: AdminUsersNew },
                              { path: ':id/edit', component: AdminUsersEdit }] }] },
    { path: '/terms', component: Terms },
    { path: '/privacy', component: Privacy },
    { path: '/contact', component: Contact }
  ]
})

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#app',
    router,
    provide: apolloProvider.provide(),
    methods: {
      activateItem(ref, index) {
        this.$refs[ref].activeIndex = index
      }
    }
  })
})
