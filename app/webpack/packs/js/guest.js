/* eslint no-console:0 */
import { Vue, VueRouter } from '../../base'
import apolloProvider from '../../plugins/apollo'

import GuestIndex from '../../pages/guest_index'
import SignIn from '../../pages/sign_in'
import SignUp from '../../pages/sign_up'
import ResetPassword from '../../pages/reset_password'
import Terms from '../../pages/terms'
import Privacy from '../../pages/privacy'
import Contact from '../../pages/contact'

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: GuestIndex },
    { path: '/sign_in', component: SignIn },
    { path: '/sign_up', component: SignUp },
    { path: '/reset_password', component: ResetPassword },
    { path: '/terms', component: Terms },
    { path: '/privacy', component: Privacy },
    { path: '/contact', component: Contact }
  ]
})

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    el: '#app',
    router,
    provide: apolloProvider.provide(),
    methods: {
      activateItem(ref, index) {
        this.$refs[ref].activeIndex = index
      }
    }
  })
})
