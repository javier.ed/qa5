import Modal from './modal'

class Ajax {
  constructor(args){
    if(!args) args = {}
    if(!args.url) args.url = location.href
    if(!args.method || !args.method.match(/^(get|post|put|delete)$/i)) args.method = 'GET'
    if(!args.data) args.data = null
    if(!args.credentials) args.credentials = 'same-origin'
    if(!args.contenttype) args.contenttype = 'application/json'
    if(!args.success) args.success = data => {
      var modal = new Modal({
        class: 'basic tiny',
        title: data.message ? data.message : 'Success',
        icon: 'check circle',
        actions: [{ class: 'ok inverted', label: 'Ok' }],
        options: { onHide: () => { modal.element.remove()
                                   if(data.location)
                                     Turbolinks.visit(data.location) }}})

      modal.open()
    }
    if(!args.error) args.error = data => {
      var modal = new Modal({
        class: 'basic tiny',
        title: data.message ? data.message : 'Error',
        icon: 'exclamation circle',
        actions: [{ class: 'ok inverted', label: 'Ok' }],
        options: { onHide: () => { modal.element.remove()
                                   if(data.location)
                                     Turbolinks.visit(data.location) }}})

      modal.open()
    }

    fetch(args.url, { method: args.method, body: args.data, credentials: args.credentials,
                      headers: new Headers({ 'Content-Type': args.contenttype}) })
      .then(response => {
        response.json()
          .then(data => {
            if(response.ok)
              args.success(data)
            else
              args.error(data)
          })
        })
      .catch(error => { console.log(error) })
  }
}

export default Ajax
