
class Modal {
  constructor(args) {
    if(!args)
      args = {};

    this.element = document.createElement('div')
    this.element.className = 'ui ' + (args.class || '') + ' modal'

    if(args.close_icon){
      var close_icon = document.createElement('i')
      close_icon.className = 'close icon'
      this.element.appendChild(close_icon)
    }

    if(args.title){
      this.title = document.createElement('div')

      if(args.icon){
        this.title.className = 'ui icon header'
        var icon = document.createElement('i')
        icon.className = args.icon + ' icon'
        this.title.appendChild(icon)
      }
      else
        this.title.className = 'header'

      this.title.innerHTML += args.title
      this.element.appendChild(this.title)
    }

    if(args.content){
      this.content = document.createElement('div')
      this.content.className = 'content'
      this.content.innerHTML = args.content
      this.element.appendChild(this.content)
    }

    if(args.actions){
      var actions = document.createElement('div')
      actions.className = 'actions'

      args.actions.forEach(action => {
        var button = document.createElement('div')
        button.className = 'ui ' + (action.class || '') + ' button'
        button.innerHTML = action.label
        actions.appendChild(button)
      })

      this.element.appendChild(actions)
    }

    var options = { onHide: () => { this.element.remove() } }
    if(args.options){
      for(var i in args.options)
        options[i] = args.options[i]
    }

    $(this.element).modal(options)
  }

  setTitle(title) {
    this.title.innerHTML = title
  }

  setContent(content) {
    this.content.innerHTML = content
  }

  open(){
    $(this.element).modal('show')
  }

  close(){
    $(this.element).modal('hode')
  }
}

export default Modal
