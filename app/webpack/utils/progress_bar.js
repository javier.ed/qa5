function startProgress() {
  if(!Turbolinks.supported) { return; }
  Turbolinks.controller.adapter.progressBar.setValue(0);
  Turbolinks.controller.adapter.progressBar.show();
}

function stopProgress() {
  if(!Turbolinks.supported) { return; }
  Turbolinks.controller.adapter.progressBar.hide();
  Turbolinks.controller.adapter.progressBar.setValue(100);
}

export default { startProgress, stopProgress }
