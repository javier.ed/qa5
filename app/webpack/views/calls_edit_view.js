import Modal from '../utils/modal'
import CallsEditTemplate from '../templates/calls_edit_template'
import Agents from '../collections/agents'
import progressBar from '../utils/progress_bar'
import Call from '../models/call'

const CallsEditView = Backbone.View.extend({
  initialize: function(args){
    progressBar.startProgress();

    this.agents = new Agents

    this.model = new Call({ id: args.id })

    this.listenTo(this.model, 'change', this.render)
    this.model.fetch()
  },

  addAgentOption: function(agent) {
    var agent_option = document.createElement('option')
    agent_option.value = agent.get('id')
    agent_option.innerHTML = agent.get('full_name')
    if(this.selected_agent_id == agent.get('id'))
      agent_option.selected = true
    document.getElementById('call_agent').appendChild(agent_option)
  },

  render: function(call) {
    this.modal = new Modal('Edit call', CallsEditTemplate({ id: call.get('id'),
                                                           made_at: call.get('made_at') }), {real_path: '/calls/' + call.get('id')})
    this.modal.show()

    this.selected_agent_id = call.get('agent_id')
    this.agents.bind('add', this.addAgentOption, this)
    this.agents.fetch()

    $('.ui.calendar.datetime').calendar({
      startMode: 'year',
      formatter: {
        date: function(date, settings) {
          return date.toISOString().slice(0,10)
        }
      }
    })

    document.getElementById('call_agent').innerHTML = ''
    var empty_option = document.createElement('option')
    empty_option.value = ''
    empty_option.innerHTML = 'Select'
    document.getElementById('call_agent').appendChild(empty_option)

    // $('.dropdown').dropdown()

    progressBar.stopProgress();
  }
});

export default CallsEditView
