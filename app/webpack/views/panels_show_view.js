import Panel from '../models/panel'
import Chart from 'chart.js/dist/Chart.min'
import PanelTableTemplate from '../templates/panel_table_template'

const PanelShowView = Backbone.View.extend({
  initialize: function (args) {
    this.el = args.el
    this.$el = args.$el

    this.model = new Panel({ id: args.id })

    this.listenTo(this.model, 'change', this.render)
    this.model.fetch()
  },

  render: function(panel) {
    if(panel.get('display_type') == 'table')
      this.el.querySelector('.description').innerHTML = PanelTableTemplate({ rows: panel.get('data_results').rows })
    else{
      this.el.querySelector('.description').innerHTML = '<canvas width="500" height="300"></canvas>'
      var canvas = this.$el.find('canvas').get(0).getContext("2d");

      var settings = {
        type: panel.get('display_type'),
        data: panel.get('data_results')
      }
      switch(panel.get('display_type')){
        case 'bar':
          settings.options = { scales: { yAxes: [ { ticks: { beginAtZero: true } } ] }, legend: { display: false } }
      }

      new Chart(canvas, settings)
    }

  }
})

export default PanelShowView
