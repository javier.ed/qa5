import Call from '../models/call'
import _ from 'underscore'

const CallsShowView = Backbone.View.extend({
  el: '#call',

  // events: {
  //   'timeupdate audio#call_audio': 'updateRadar'
  // },

  radar_chart: null,
  segments: [],

  initialize: function(args){
    this.model = new Call({ id: args.id })

    this.listenTo(this.model, 'change', this.render)
    this.model.fetch()

    _.bindAll(this, 'updateRadar');
    this.$('audio#call_audio').on('timeupdate', this.updateRadar);
  },

  updateRadar: function() {
    var audio = this.el.querySelector('audio#call_audio')
    audio.currentTime

    for(var i in this.segments) {
      if(this.segments[i].start <= audio.currentTime && this.segments[i].end >= audio.currentTime){
        var current_segment = this.segments[i]
    
        var data = [ current_segment.angry, current_segment.upset, current_segment.stress, current_segment.concentration,
                     current_segment.content, current_segment.intensive_thinking, current_segment.energy ]

        this.radar_chart.data.datasets[0].data = data
        this.radar_chart.update();
      }
    }
  },

  render: function(call) {
    var canvas = this.$el.find('canvas#timeline_radar').get(0).getContext("2d")

    this.segments = call.get('segments')

    var current_segment = this.segments[0]
    
    var data = [ current_segment.angry, current_segment.upset, current_segment.stress, current_segment.concentration,
                 current_segment.content, current_segment.intensive_thinking, current_segment.energy ]

    var data = { datasets: [ { data: data } ], labels: ['Angry', 'Upset', 'Stress', 'Concentration', 'Content', 'I. thinking', 'Energy'] }

    var settings = { type: 'radar', data: data,
      options: { legend: { display: false }, scale: { ticks: { display: false },pointLabels: { fontSize: 13 } } } }

    this.radar_chart = new Chart(canvas, settings)
  }
})

export default CallsShowView
