import Modal from '../utils/modal'
import CallsNewTemplate from '../templates/calls_new_template'
import Agents from '../collections/agents'
import progressBar from '../utils/progress_bar'

const CallsNewView = Backbone.View.extend({
  initialize: function(args){
    progressBar.startProgress();

    this.agents = new Agents

    this.agents.bind('add', this.addAgentOption, this)
    this.agents.fetch()

    this.render()
  },

  addAgentOption: function(agent) {
    var agent_option = document.createElement('option')
    agent_option.value = agent.get('id')
    agent_option.innerHTML = agent.get('full_name')
    document.getElementById('call_agent').appendChild(agent_option)
  },

  render: function() {
    this.modal = new Modal('Add call', CallsNewTemplate(), {real_path: '/calls'})

    this.modal.show()

    $('.ui.calendar.datetime').calendar({
      startMode: 'year',
      formatter: {
        date: function(date, settings) {
          return date.toISOString().slice(0,10)
        }
      }
    })

    document.getElementById('call_agent').innerHTML = ''
    var empty_option = document.createElement('option')
    empty_option.value = ''
    empty_option.innerHTML = 'Select'
    document.getElementById('call_agent').appendChild(empty_option)

    // $('.dropdown').dropdown()

    progressBar.stopProgress();
  }
});

export default CallsNewView
