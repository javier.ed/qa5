import progressBar from '../utils/progress_bar'
import PanelLocation from '../models/panel_location'

const PanelsNewView = Backbone.View.extend({
  el: '#new_panel',

  events: {
    'change #panel_template_panel_id': 'changeTemplate'
  },

  templates: {},

  display_type_labels: { table: 'Table', pie: 'Pie chart', bar: 'Bar chart', line: 'Line chart',
                         row: 'Rows', portion: 'Portions', column: 'Columns', line: 'Lines', point: 'Points' },

  initialize: function(args) {
    progressBar.startProgress()

    this.modal = args.modal

    this.model = new PanelLocation({id: 'dashboard'})

    this.listenTo(this.model, 'change', this.render)
    this.model.fetch()
  },

  changeTemplate: function(){
    var template_select = this.el.querySelector('#panel_template_panel_id')
    var fields_options = this.el.querySelector('#fields_options')
    while(fields_options.lastChild)
      fields_options.removeChild(fields_options.lastChild)

    for(var i in this.templates) {
      if (this.templates[i].id  == template_select.value)
        var template = this.templates[i]
    }

    var fields = {}

    for(var j in template.options) {
      var option = template.options[j]

      if(!fields[option.display_type]){
        fields[option.display_type] = document.createElement('div')
        fields[option.display_type].id = 'fields_' + option.display_type
        var title = document.createElement('label')
        title.innerHTML = this.display_type_labels[option.display_type]
        fields[option.display_type].appendChild(title)
        fields_options.appendChild(fields[option.display_type])
      }

      var input = document.createElement('input')
      input.type = 'checkbox'
      input.name = 'panel[options_attributes][][template_panel_option_id]'
      input.id = 'panel_options_attributes_' + j + '_template_panel_option_id'
      input.value = option.id
      
      var label = document.createElement('label')
      label.setAttribute('for', 'panel_options_attributes_' + j + '_template_panel_option_id')
      label.innerHTML = option.label

      var div_toggle = document.createElement('div')
      div_toggle.className = 'ui toggle checkbox'
      div_toggle.appendChild(input)
      div_toggle.appendChild(label)

      var div_field = document.createElement('div')
      div_field.className = 'field'
      div_field.appendChild(div_toggle)

      fields[option.display_type].appendChild(div_field)
    }
  },

  render: function(panel) {
    this.templates = panel.get('templates')

    this.modal.show()

    var template_select = this.el.querySelector('#panel_template_panel_id')

    for(var i in this.templates) {
      var template_option = document.createElement('option')
      template_option.value = this.templates[i].id
      template_option.innerHTML = this.templates[i].label +
      ' (' + this.display_type_labels[this.templates[i].display_type] + ')'
      template_select.appendChild(template_option)
    }

    progressBar.stopProgress()
  }
})

export default PanelsNewView
