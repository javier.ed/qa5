import Panel from "../models/panel";
import PanelShowView from "./panels_show_view";

const HomeView = Backbone.View.extend({
  el: '#panels',

  initialize: function (args) {
    
    this.render()
  },

  render: function() {
    var panels = this.el.querySelectorAll('.card')
    
    for(var i in panels){
      if(typeof(panels[i].id) != 'undefined'){
        new PanelShowView({ el: panels[i], $el: $(panels[i]), id: panels[i].id.split('_')[1] })
      }
    }
  }
})

export default HomeView
