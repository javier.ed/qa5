/* eslint no-console:0 */
import Vue from 'vue/dist/vue.esm'
import VueRouter from 'vue-router'
import locale from 'element-ui/lib/locale/lang/en'
import VueApollo from 'vue-apollo'
import ElementUI from 'element-ui'

Vue.use(VueRouter)

Vue.use(VueApollo)

Vue.use(ElementUI, { locale })

export { Vue, VueRouter }
