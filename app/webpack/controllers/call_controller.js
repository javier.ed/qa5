import { Controller } from 'stimulus'
import Ajax from '../utils/ajax'

export default class extends Controller {
    static targets = ['radar', 'audio']
    connect(){
        new Ajax({
            url: location.pathname + '.json',
            success: data => {
                this.canvas = $(this.radarTarget).get(0).getContext("2d")
                this.segments = data.segments
                var current_segment = this.segments[0]
                
                var data = [ current_segment.angry, current_segment.upset, current_segment.stress, current_segment.concentration,
                            current_segment.content, current_segment.intensive_thinking, current_segment.energy ]

                var data = { datasets: [ { data: data } ], labels: ['Angry', 'Upset', 'Stress', 'Concentration', 'Content', 'I. thinking', 'Energy'] }

                var settings = { type: 'radar', data: data,
                options: { legend: { display: false }, scale: { ticks: { display: false },pointLabels: { fontSize: 13 } } } }

                this.radar_chart = new Chart(this.canvas, settings)

                this.audioTarget.addEventListener('timeupdate', () => {
                    for(var i in this.segments) {
                        if(this.segments[i].start <= this.audioTarget.currentTime && this.segments[i].end >= this.audioTarget.currentTime){
                            var current_segment = this.segments[i]
                        
                            var data = [ current_segment.angry, current_segment.upset, current_segment.stress, current_segment.concentration,
                                        current_segment.content, current_segment.intensive_thinking, current_segment.energy ]

                            this.radar_chart.data.datasets[0].data = data
                            this.radar_chart.update();
                        }
                    }
                })
            }
        })
    }
}
