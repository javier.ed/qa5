class Qa5Schema < GraphQL::Schema
  query Types::QueryType
  mutation Types::MutationType
  context_class Contexts::CustomContext

  def self.id_from_object(object, type, ctx)
    object.id.to_s
  end
end
