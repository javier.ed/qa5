class Types::EmailType < GraphQL::Schema::Object
  global_id_field :id
  field :email, String, null: false
  field :verified, Boolean, null: true
  field :primary, Boolean, null: true
end
