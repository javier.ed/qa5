class Types::UserType < GraphQL::Schema::Object
  global_id_field :id
  field :username, String, null: false
  field :created_at, String, null: true
  field :role, String, null: true
  field :profile, Types::ProfileType, null: true
  field :emails, [Types::EmailType], null: true
  field :panels, [Types::UserPanelType], null: true do
    argument :sort, String, required: false
  end

  def panels(sort:)
    self.object.panels.order(sort) if sort
  end
end
