class Types::UserPanelType < Types::BaseType
  global_id_field :id

  field :position, Integer, null: true

  field :panel, Types::PanelType, null: true do
    argument :location, String, required: false
  end

  def panel(location:)
    # Panel.find_by(id: self.object.panel_id, location: location)
    self.object.panel if self.object.panel.location == location || !location
  end
end
