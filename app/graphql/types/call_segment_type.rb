class Types::CallSegmentType < Types::BaseType
  global_id_field :id
  field :created_at, String, null: true
  
  field :number, Integer, null: true
  field :start, Float, null: true
  field :end, Float, null: true
  field :voice_energy, Integer, null: true
  field :online_lva, String, null: true
  field :energy, Float, null: true
  field :content, Float, null: true
  field :upset, Float, null: true
  field :angry, Float, null: true
  field :stress, Float, null: true
  field :concentration_level, Float, null: true
  field :emocog_ratio, Float, null: true
  field :hesitation, Float, null: true
  field :intensive_thinking, Float, null: true
  field :imagination_activity, Float, null: true
  field :saf, Float, null: true
end