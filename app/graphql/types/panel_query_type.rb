class Types::PanelQueryType < Types::BaseType
  global_id_field :id
  field :name, String, null: true
  field :query, String, null: true
end
