class Types::CallType < Types::BaseType
  global_id_field :id
  field :created_at, String, null: true
  field :made_at, String, null: true
  field :file, String, null: true
  field :file_name, String, null: true
  field :file_status, String, null: true
  field :online_flag, String, null: true
  field :duration, String, null: true

  field :agent_id, ID, null: true
  field :agent, Types::UserType, null: true

  field :segments, [Types::CallSegmentType], null: true

  def made_at
    self.object.made_at.localtime if self.object.made_at
  end
end
