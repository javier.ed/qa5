class Types::PanelType < Types::BaseType
  global_id_field :id
  field :name, String, null: true
  field :location, String, null: true
  field :display_type, String, null: true
  field :queries, [Types::PanelQueryType], null: true
end
