class Types::ProfileType < GraphQL::Schema::Object
  global_id_field :id
  field :first_name, String, null: true
  field :last_name, String, null: true
  field :full_name, String, null: true
  field :gender, String, null: true
  field :birthdate, String, null: true
  field :country_code, String, null: true
end
