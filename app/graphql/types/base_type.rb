class Types::BaseType < GraphQL::Schema::Object
  field :success, Boolean, null: true
  field :message, String, null: true
  field :errors, [Types::ErrorType], null: true
end