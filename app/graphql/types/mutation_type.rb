class Types::MutationType < GraphQL::Schema::Object
  field :signUp, field: Mutations::SignUp.field, null: true
  field :signIn, field: Mutations::SignIn.field, null: true
  field :signOut, field: Mutations::SignOut.field, null: true

  field :share, field: Mutations::Share.field, null: true

  field :createUserPanel, field: Mutations::CreateUserPanel.field, null: true
  field :updateUserPanel, field: Mutations::UpdateUserPanel.field, null: true
  field :deleteUserPanel, field: Mutations::DeleteUserPanel.field, null: true

  field :updateProfile, field: Mutations::UpdateProfile.field, null: true

  field :createEmail, field: Mutations::CreateEmail.field, null: true
  field :updateEmail, field: Mutations::UpdateEmail.field, null: true
  field :deleteEmail, field: Mutations::DeleteEmail.field, null: true

  field :updatePassword, field: Mutations::UpdatePassword.field, null: true

  field :updateCall, field: Mutations::UpdateCall.field, null: true
  field :deleteCall, field: Mutations::DeleteCall.field, null: true
  
  field :createPanel, field: Mutations::CreatePanel.field, null: true
  field :updatePanel, field: Mutations::UpdatePanel.field, null: true
  field :deletePanel, field: Mutations::DeletePanel.field, null: true

  field :createUser, field: Mutations::CreateUser.field, null: true
  field :updateUser, field: Mutations::UpdateUser.field, null: true
  field :deleteUser, field: Mutations::DeleteUser.field, null: true
end
