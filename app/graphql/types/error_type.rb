class Types::ErrorType < GraphQL::Schema::Object
  field :field, String, null: false
  field :message, String, null: false
end