class Types::QueryType < GraphQL::Schema::Object
  field :users, [Types::UserType], null: true do
    argument :role, String, required: false
  end

  def users(role:)
    if role
      User.where(role: role)
    else
      User.all
    end
  end

  field :user, Types::UserType, null: true do
    argument :id, ID, required: true
  end

  def user(id:)
    User.where(username: id).or(User.where(id: id)).first
  end

  field :current_user, Types::UserType, null: true

  def current_user
    context.current_user
  end

  field :panels, [Types::PanelType], null: true do
    argument :location, String, required: false
  end

  def panels(location:)
    if location
      Panel.where(location: location)
    else
      Panel.all
    end
  end

  field :panel, Types::PanelType, null: true do
    argument :id, ID, required: true
  end

  def panel(id:)
    Panel.find(id)
  end

  field :calls, [Types::CallType], null: true

  def calls
    Call.all
  end

  field :call, Types::CallType, null: true do
    argument :id, ID, required: true
  end

  def call(id:)
    Call.find(id)
  end
end
