class Mutations::CreateEmail < Mutations::Base
  argument :email, Inputs::EmailInput, required: true

  field :email, Types::EmailType, null: true

  def resolve(email:)
    authenticate_user!
    resource = current_user.emails.build(email: email.email, primary: email.primary)
    if resource.save
      # EmailsMailer.verification(resource.email, current_user.username, resource.id.to_s).deliver_later
      { success: true, message: 'Email added successfully', email: resource }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to add email', errors: errors }
    end
  end
end
