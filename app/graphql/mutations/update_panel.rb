class Mutations::UpdatePanel < Mutations::Base
  argument :panel, Inputs::PanelInput, required: true

  def resolve(panel:)
    authenticate_user!
    resource = Panel.find(panel.id)

    resource.name = panel.name
    resource.location = panel.location
    resource.display_type = panel.display_type

    query_ids = []
    panel.queries.each do |q|
      query_ids.append(q.id)
    end
    resource.queries.where('id NOT IN (?)', query_ids).destroy_all

    panel.queries.each do |query|
      if query.id.to_i > 0
        query_ids.append(query.id)
        resource.queries.find(query.id).update(name: query.name, query: query.query)
      else
        resource.queries.build(name: query.name, query: query.query)
      end
    end

    if resource.save
      { success: true, message: 'Panel updated successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to update panel', errors: errors }
    end
  end
end
