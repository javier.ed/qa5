class Mutations::UpdateCall < Mutations::Base
  argument :call, Inputs::CallInput, required: true

  field :call, Types::CallType, null: true

  def resolve(call:)
    authenticate_user!
    resource = current_user.calls.find(call.id)
    if resource.update({ made_at: call.made_at, agent_id: call.agent_id })
      { success: true, message: 'Call updated successfully', call: resource }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      { success: false, message: 'Failed to update call', errors: errors }
    end
  end
end
