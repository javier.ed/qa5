class Mutations::DeleteCall < Mutations::Base
  argument :call, Inputs::CallInput, required: true

  def resolve(call:)
    authenticate_user!
    resource = current_user.calls.find(call.id)
    if resource.destroy
      { success: true, message: 'Call deleted successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      { success: false, message: 'Failed to delete call', errors: errors }
    end
  end
end
