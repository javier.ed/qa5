class Mutations::UpdateProfile < Mutations::Base
  argument :profile, Inputs::ProfileInput, required: true

  def resolve(profile:)
    authenticate_user!
    resource = current_user.profile
    if resource.update_attributes(first_name: profile.first_name, last_name: profile.last_name,
                                  gender: profile.gender, birthdate: profile.birthdate,
                                  country_code: profile.country_code)

      { success: true, message: 'Profile updated successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to update profile', errors: errors }
    end
  end
end