class Mutations::UpdateUserPanel < Mutations::Base
  argument :user_panel, Inputs::UserPanelInput, required: true

  field :user_panel, Types::UserPanelType, null: true

  def resolve(user_panel:)
    authenticate_user!
    resource = current_user.panels.find(user_panel.id)
    if resource.update(move_up: user_panel.move_up, move_down: user_panel.move_down)
      {success: true, message: 'Panel updated successfully', user_panel: resource }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to update panel', errors: errors }
    end
  end
end
