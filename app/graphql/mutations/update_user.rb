class Mutations::UpdateUser < Mutations::Base
  argument :user, Inputs::UserInput, required: true

  def resolve(user:)
    authenticate_user!
    resource = User.find(user.id)

    if resource.update(role: user.role)
      { success: true, message: 'User updated successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      { success: false, message: 'Failed to update user', errors: errors }
    end
  end
end