class Mutations::UpdatePassword < Mutations::Base
  include Devise::Controllers::SignInOut

  argument :password, Inputs::PasswordInput, required: true
  
  def resolve(password:)
    authenticate_user!
    resource = current_user
    if current_user.update_with_password({ current_password: password.current_password,
                                           password: password.password,
                                           password_confirmation: password.password_confirmation })
      bypass_sign_in current_user
      { success: true, message: 'Password changed successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      { success: false, message: 'Failed to change password', errors: errors }
    end
  end
end
