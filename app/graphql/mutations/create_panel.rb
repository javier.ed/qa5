class Mutations::CreatePanel < Mutations::Base
  argument :panel, Inputs::PanelInput, required: true

  def resolve(panel:)
    authenticate_user!
    resource = Panel.new(name: panel.name, location: panel.location, display_type: panel.display_type)

    panel.queries.each do |query|
      resource.queries.build(name: query.name, query: query.query)
    end

    if resource.save
      { success: true, message: 'Panel created successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to create panel', errors: errors }
    end
  end
end
