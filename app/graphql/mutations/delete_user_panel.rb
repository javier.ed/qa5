class Mutations::DeleteUserPanel < Mutations::Base
  argument :user_panel, Inputs::UserPanelInput, required: true

  field :user_panel, Types::UserPanelType, null: true

  def resolve(user_panel:)
    authenticate_user!
    resource = current_user.panels.find(user_panel.id)
    if resource.delete
      { success: true, message: 'Panel deleted successfully', user_panel: resource }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to add panel', errors: errors }
    end
  end
end