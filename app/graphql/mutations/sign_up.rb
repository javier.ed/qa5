class Mutations::SignUp < Mutations::Base
  argument :user, Inputs::SignUpInput, required: true

  def resolve(user:)
    resource = User.new(username: user.username, password: user.password, password_confirmation: user.password_confirmation,
                        terms: user.terms)

    user.emails.each do |email|
      resource.emails.build(email: email.email)
    end

    if user.profile
      resource.profile = Profile.new(first_name: user.profile.first_name, last_name: user.profile.last_name,
                                     gender: user.profile.gender, birthdate: user.profile.birthdate, country_gec: user.profile.countryGec)
    end

    if resource.save
      # SignUpInvitation.find(resource.invite)&.update(used_at: Time.current) if resource.invite.present?
      RegistrationsMailer.welcome(resource.email, resource.username).deliver_later
      resource.emails.each do |email|
        EmailsMailer.verification(email.email, resource.username, email.id.to_s).deliver_later
      end

      { success: true, message: 'User created successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      
      if resource.emails
        i = 0
        resource.emails.each do |email|
          errors.concat email.errors.collect {|key, value| { field: "emails_#{i}_#{key}", message: value } }
          i += 1
        end
      end
      
      errors.concat(resource.profile.errors.collect {|key, value| { field: "profile_#{key}", message: value } }) if resource.profile
      
      { success: false, message: 'Failed to create user', errors: errors }
    end
  end
end
