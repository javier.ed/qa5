class Mutations::SignOut < Mutations::Base
  include Devise::Controllers::SignInOut

  def resolve()
    if current_user && sign_out(:user)
      { success: true, message: 'Signed out successfully' }
    else
      { success: false, message: 'User session not fount' }
    end
  end
end