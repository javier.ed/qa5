class Mutations::DeleteUser < Mutations::Base
  argument :user, Inputs::UserInput, required: true

  def resolve(user:)
    authenticate_user!
    resource = User.find(user.id)

    if resource.destroy
      { success: true, message: 'User deleted successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      { success: false, message: 'Failed to delete user', errors: errors }
    end
  end
end
