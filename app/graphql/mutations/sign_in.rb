class Mutations::SignIn < Mutations::Base
  include Devise::Controllers::SignInOut
  include Devise::Controllers::Rememberable

  argument :user, Inputs::SignInInput, required: true

  def resolve(user:)
    return { success: false, message: 'Already signed in' } if current_user

    resource = user.password.present? && User.find_for_database_authentication(login: user.login)

    if resource && resource.valid_password?(user.password)
      sign_in(:user, resource)
      remember_me(resource) if user.remember_me
      { success: true, message: 'Signed in successfully' }
    else
      { success: false, message: 'Login or password incorrect' }
    end
  end
end
