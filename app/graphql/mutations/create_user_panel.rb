class Mutations::CreateUserPanel < Mutations::Base
  argument :user_panel, Inputs::UserPanelInput, required: true

  field :user_panel, Types::UserPanelType, null: true

  def resolve(user_panel:)
    authenticate_user!
    resource = current_user.panels.build(panel_id: user_panel.panel_id)
    if resource.save
      { success: true, message: 'Panel added successfully', user_panel: resource }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to add panel', errors: errors }
    end
  end
end