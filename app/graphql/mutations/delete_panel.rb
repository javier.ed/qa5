class Mutations::DeletePanel < Mutations::Base
  argument :panel, Inputs::PanelInput, required: true

  def resolve(panel:)
    authenticate_user!
    resource = Panel.find(panel.id)

    if resource.destroy
      { success: true, message: 'Panel deleted successfully' }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }

      { success: false, message: 'Failed to delete panel', errors: errors }
    end
  end
end
