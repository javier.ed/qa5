class Mutations::DeleteEmail < Mutations::Base
  argument :email, Inputs::EmailInput, required: true

  field :email, Types::EmailType, null: true

  def resolve(email:)
    authenticate_user!
    resource = current_user.emails.find(email.id)
    if resource.delete
      { success: true, message: 'Email deleted successfully' }
    else
      { success: false, message: 'Failed to delete email' }
    end
  end
end
