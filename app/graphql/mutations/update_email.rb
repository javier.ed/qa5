class Mutations::UpdateEmail < Mutations::Base
  argument :email, Inputs::EmailInput, required: true

  field :email, Types::EmailType, null: true

  def resolve(email:)
    authenticate_user!
    resource = current_user.emails.find(email.id)
    if !email.primary || resource.update_attributes(primary: true)
      EmailsMailer.verification(resource.email, current_user.username, resource.id.to_s).deliver_later if email.resend_verification
      { success: true, message: 'Email updated successfully', email: resource }
    else
      errors = resource.errors.collect {|key, value| { field: key, message: value } }
      { success: false, message: 'Failed to update email', errors: errors }
    end
  end
end
