class Mutations::Share < Mutations::Base
  argument :share, Inputs::ShareInput, required: true

  def resolve(share:)
    ShareMailer.share(share.email, generate_pdf.render).deliver_later
    { success: true, message: 'Email sended succesfully' }
  end
end
