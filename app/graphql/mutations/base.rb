class Mutations::Base < GraphQL::Schema::Mutation
  field :success, Boolean, null: true
  field :message, String, null: true
  field :errors, [Types::ErrorType], null: true

  def session
    context.session
  end

  def cookies
    context.cookies
  end

  def request
    context.request
  end

  def warden
    request.env['warden']
  end

  def current_user
    context.current_user
  end

  def authenticate_user!
    { success: false, message: 'You need to sign in to continue' } unless current_user
  end
end
