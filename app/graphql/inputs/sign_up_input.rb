class Inputs::SignUpInput < GraphQL::Schema::InputObject
  argument :username, String, required: false
  argument :password, String, required: false
  argument :password_confirmation, String, required: false
  argument :role, String, required: false

  argument :emails, [Inputs::EmailInput], required: false

  argument :profile, Inputs::ProfileInput, required: false

  argument :terms, Boolean, required: false
end
