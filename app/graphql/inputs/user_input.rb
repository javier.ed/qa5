class Inputs::UserInput < GraphQL::Schema::InputObject
  argument :id, ID, required: false
  argument :role, String, required: false
end
