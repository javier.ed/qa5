class Inputs::CallInput < GraphQL::Schema::InputObject
  argument :id, ID, required: false
  argument :agent_id, ID, required: false
  argument :made_at, String, required: false
end