class DeleteAccountInput < GraphQL::Schema::InputObject
  argument :username, String, required: false
  argument :password, String, required: false
end