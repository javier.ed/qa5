class Inputs::PasswordInput < GraphQL::Schema::InputObject
  argument :current_password, String, required: false
  argument :password, String, required: false
  argument :password_confirmation, String, required: false
end
