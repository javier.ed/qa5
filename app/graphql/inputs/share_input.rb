class Inputs::ShareInput < GraphQL::Schema::InputObject
  argument :email, String, required: true
  argument :time_range, String, required: false
end