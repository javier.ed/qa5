class Inputs::QueryInput < GraphQL::Schema::InputObject
  argument :id, ID, required: false
  argument :name, String, required: false
  argument :query, String, required: false
end
