class Inputs::ProfileInput < GraphQL::Schema::InputObject
  argument :first_name, String, required: false
  argument :last_name, String, required: false
  argument :gender, String, required: false
  argument :birthdate, String, required: false
  argument :country_code, String, required: false
end
