class Inputs::EmailInput < GraphQL::Schema::InputObject
  argument :id, ID, required: false
  argument :email, String, required: false
  argument :primary, Boolean, required: false
  argument :resend_verification, Boolean, required: false
end
