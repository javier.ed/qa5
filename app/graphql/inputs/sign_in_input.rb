class Inputs::SignInInput < GraphQL::Schema::InputObject
  argument :login, String, required: true
  argument :password, String, required: true
  argument :remember_me, Boolean, required: false
end