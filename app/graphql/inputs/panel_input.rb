class Inputs::PanelInput < GraphQL::Schema::InputObject
  argument :id, ID, required: false
  argument :name, String, required: false
  argument :location, String, required: false
  argument :display_type, String, required: false
  argument :queries, [Inputs::QueryInput], required: false
end
