class Inputs::UserPanelInput < GraphQL::Schema::InputObject
  argument :id, ID, required: false
  argument :panel_id, String, required: false
  argument :move_up, Boolean, required: false
  argument :move_down, Boolean, required: false
end
