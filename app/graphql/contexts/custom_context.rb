class Contexts::CustomContext < GraphQL::Query::Context
  def current_user
    self[:current_user]
  end

  def current_user=(new_current_user)
    self[:current_user] = new_current_user
  end

  def session
    self[:session]
  end

  def cookies
    self[:cookies]
  end

  def request
    self[:request]
  end

  # def inspect
  #   "#<CustomContext viewer=#{current_user&.screenname.inspect}"
  # end
end
