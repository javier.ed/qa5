class ShareMailer < ApplicationMailer
  def share(email, file)
    attachments['dashboard.pdf'] = file
    mail to: email, subject: "Dashboard"
  end
end
