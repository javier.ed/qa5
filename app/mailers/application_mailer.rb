class ApplicationMailer < ActionMailer::Base
  default from: Settings.name + ' <' + Settings.mail[:sender_address] + '>'
  layout 'mailer'
end
