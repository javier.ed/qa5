class LvaJob < ApplicationJob
  queue_as :default

  def perform(*calls)
    calls.each do |call|
      next if call.file_status == 'waiting'

      call.file_statuses.destroy_all
      call.segments.destroy_all

      call.file_statuses.create(status: 'waiting')

      param_names = { 'globalseg' => :global_segment, 'seg' => :number, 'topic' => :topic, 'channel' => :channel,
                      'cnl' => :channel, 'startpos' => :start, 'endpos' => :end, 'startpossec' => :start,
                      'endpossec' => :end, 'onlinelva' => :online_lva, 'offlinelva' => :offline_lva,
                      'risk1' => :risk1, 'risk2' => :risk2, 'riskoz' => :riskoz, 'oz1oz2oz3' => :oz1oz2oz3,
                      'energy' => :energy, 'content' => :content, 'upset' => :upset, 'angry' => :angry, 'stressed' => :stress,
                      'uncertain' => :uncertain, 'excited' => :excited, 'coglevel' => :cognitive_level,
                      'emolevel' => :emotional_level, 'concentration' => :concentration_level,
                      'concentrated' => :concentration_level, 'anticipation' => :anticipation, 'hesitation' => :hesitation,
                      'emobalance' => :emotional_balance, 'ithink' => :intensive_thinking, 'imagin' => :imagination_activity,
                      'saf' => :saf, 'oca' => :brain_power, 'branpower' => :brain_power, 'embar' => :embarrassment,
                      'atmos' => :atmosphere, 'emocogratio' => :emocog_ratio, 'extremeemotion' => :extreme_emotion,
                      'coghighlowbalance' => :cognition_high_low_balance, 'voiceenergy' => :voice_energy,
                      'lvariskstress' => :lva_risk_stress, 'lvaglbstress' => :lva_global_stress,
                      'lvaemostress' => :lva_emotional_stress, 'lvacogstress' => :lva_cognitive_stress,
                      'lvaenrstress' => :lva_energetic_stress, 'lionetanalysis' => :lionet_analysis,
                      'onlineflag' => :online_flag, 'comment' => :comment, 'lionetinfo' => :lionet_info }

      if call.csv_file
        sheet = Roo::Spreadsheet.open(call.csv_file)
        sheet.default_sheet = sheet.sheets[0]
        columns = sheet.row(sheet.first_row).collect { |k| k.gsub(/[^0-9A-Za-z]/, '').downcase if k }

        sheet.drop(1).each do |row|
          segment_data = {}
          row.each_index do |i|
            segment_data[param_names[columns[i]]] = row[i] if param_names[columns[i]]
          end

          call.segments.create(segment_data)
        end

        call.duration = call.segments.last.end
        if call.segments.last.offline_lva
          call.tag = call.segments.last.offline_lva
          tag = call.tag.split(' (')
          call.tag_name = tag[0]
          call.tag_value = tag[1][0..-2]
        end
        call.save

        call.file_statuses.create(status: 'done')
        return
      elsif call.file
        url = "http://rservice.nemesysco.net/RAnalysis/?actionType=2&audioStreamURL=#{Settings.protocol}://#{Settings.host}:#{Settings.port.to_s+call.file.url}"
        uri = URI.parse(url)

        request = Net::HTTP::Get.new(uri)
        request['N-MS-AUTHCB'] = Settings.lva[:api_key]

        response = Net::HTTP.start(uri.hostname, uri.port) do |http|
          http.request(request)
        end

        body_json = JSON.parse(response.body)

        case response
        when Net::HTTPSuccess
          if body_json['Status'] != 'ERROR'
            columns = body_json['Data']['SEG'].delete_at(0).split(';').collect { |k| k.gsub(/[^0-9A-Za-z]/, '').downcase if k }
  
            body_json['Data']['SEG'].each do |segment|
              if segment
                segment_data = {}
                row = segment.split(';')
                row.each_index do |i|
                  segment_data[param_names[columns[i]]] = row[i] if param_names[columns[i]]
                end
                
                call.segments.create(segment_data)
              end
            end
  
            call.duration = call.segments.last.end
            call.tag = call.segments.last.offline_lva
            tag = call.tag.split(' (')
            call.tag_name = tag[0]
            call.tag_value = tag[1][0..-2]
            call.save
  
            call.file_statuses.create(status: 'done')
            return
          end
        end
      end

      call.file_statuses.create(status: 'rejected')
    end
  end
end
