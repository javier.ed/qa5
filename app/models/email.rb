class Email < ApplicationRecord
  belongs_to :user

  validates :email, presence: true
  validates :email, uniqueness: { case_sensitive: false }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, allow_blank: true }

  def verified
    true if verified_at
  end

  # before_create :set_primary_unless_there_is_another, :unset_another_primary_if_this_is_set

  protected

  def unset_another_primary_if_this_is_set
    user.emails.find_by(:id.ne => id, primary: true)&.unset(:primary)&.save if primary
  end

  def set_primary_unless_there_is_another
    self.primary = true unless user&.emails&.find_by(primary: true)
  end
end
