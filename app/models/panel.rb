class Panel < ApplicationRecord
  has_many :user_panels, dependent: :destroy
  has_many :queries, class_name: 'PanelQuery', inverse_of: :panel, dependent: :destroy

  accepts_nested_attributes_for :queries

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }, allow_blank: true
  validates :location, inclusion: { in: %w[home calls agents] }
  validates :display_type, inclusion: { in: %w[table pie bar line column area scatter] }
  validates_associated :queries

  def run_query(args = {})
    args = args.with_indifferent_access
    args[:start_time] = "'#{Time.parse('2000-01-01')}'" unless args[:start_time]
    args[:end_time] = "'#{Time.current}'" unless args[:end_time]
    generated_query = query.gsub Regexp.compile(/:(\w+)/i) do
      %( #{replace_arg(Regexp.last_match(1), args)} )
    end
    ActiveRecord::Base.connection.exec_query(generated_query)
  end

  def run_queries(args = {})
    results = []

    queries.each do |query|
      if query.name.present? && ['line', 'bar', 'column', 'scatter', 'area'].include?(display_type)
        results.append name: query.name, data: query.run_query(args)
      else
        results.concat query.run_query(args)
      end
    end

    return results
  end
end
