class UserPanel < ApplicationRecord
  belongs_to :user
  belongs_to :panel

  validates :panel, presence: true

  def template
    panel
  end

  def title
    panel.name
  end

  def display_type
    panel.display_type
  end

  def move_up=(value)
    self.position = 0 if self.position.nil?
    self.position += 1 if value
  end

  def move_down=(value)
    self.position = 0 if self.position.nil?
    self.position -= 1 if value
  end

  def to_h
    attributes.merge(data_results: data_results, display_type: display_type)
  end
end
