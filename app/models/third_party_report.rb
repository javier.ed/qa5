class ThirdPartyReport < ApplicationRecord
  belongs_to :user
  has_many :calls

  mount_uploader :file, FileUploader

  before_create :create_calls

  private

  def create_calls
    sheet = Roo::Spredsheet.open(file)
    sheet.default_sheet = sheet.sheets[0]
    columns = sheet.row(sheet.first_row).collect { |k| k.gsub(/[^0-9A-Za-z]/, '').downcase if k }

    param_names = { 'filename' => :file_name, 'calltag' => :tag, 'duration' => :duration,
                    'highestqualitypriority' => :highest_quality_priority, 'finalqualitypriority' => :final_quality_priority,
                    'agentpriorityrank' => :agent_priority_rank, 'dissatisfactionscore' => :dissatisfaction_score,
                    'avrgvoiceenergy' => :avg_voice_energy, 'anger' => :anger_percentage, 'stress' => :stress_percentage,
                    'upset' => :upset_percentage, 'content' => :content_percentage, 'avemotionalenergy' => :avg_emotional_energy,
                    'energylevel' => :energy_level, 'arousallevel' => :arousal_level, 'emotionlevel' => :emotion_level,
                    'uneasylevel' => :uneasy_level, 'stresslevel' => :stress_level, 'thinkinglevel' => :thinking_level,
                    'confidence' => :confidence, 'concentration' => :concentration, 'anticipation' => :anticipation,
                    'cscscore' => :csc_score, 'fi10score' => :fi10_score, 'fileformat' => :file_format }

    sheet.drop(1).each do |row|
      call_data = {}
      row.each_index do |i|
        call_data[param_names[columns[i]]] = row[i] if param_names[columns[i]]
      end
      self.calls.build(call_data)
    end
  end
end
