class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :login, :terms, :change_password, :invite

  has_one :profile, dependent: :destroy
  has_many :emails, dependent: :destroy

  has_many :calls, inverse_of: :user, dependent: :nullify
  has_many :calls_made, class_name: 'Call', inverse_of: :agent, dependent: :nullify

  has_many :third_party_reports, dependent: :nullify

  has_many :panels, class_name: 'UserPanel', dependent: :destroy

  accepts_nested_attributes_for :profile, :emails

  validates :username, presence: true
  validates :username, length: { minimum: 5, maximum: 20 }, allow_blank: true
  validates :username, format: { with: /\A[A-Za-z0-9_\-.]+\z/i }, allow_blank: true
  validates :username, exclusion: { in: Settings.create_user['username_blacklist'] }
  validates :username, uniqueness: { case_sensitive: false }
  validates :terms, acceptance: true, on: :create
  validates :role, presence: true
  validates :role, inclusion: { in: %w[user agent manager administrator superuser] }, allow_blank: true
  validate :validate_only_superuser
  validate :validate_change_password, if: :change_password
  validates_associated :profile, :emails

  def email
    primary_email = emails.find_by(primary: true)
    return primary_email.email if primary_email
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

  def superuser?
    true if role == 'superuser'
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    login = conditions.delete(:login)
    where(conditions).where(["lower(username) = :value", {value: login.strip.downcase}]).first
  end

  protected

  def validate_only_superuser
    return true unless role == 'superuser' && User.where('role = "superuser" AND id != ?', id).first
    errors.add(:role, 'Only one superuser allowed')
    false
  end

  def validate_change_password
    return true if password.present?
    errors.add(:password, :blank)
    false
  end
end
