class Call < ApplicationRecord
  belongs_to :agent, class_name: 'User', inverse_of: :calls_made, optional: true
  belongs_to :user, inverse_of: :calls
  belongs_to :third_party_report, optional: true

  has_many :segments, class_name: 'CallSegment', dependent: :destroy
  has_many :file_statuses, class_name: 'CallFileStatus', dependent: :destroy

  mount_uploader :file, FileUploader
  mount_uploader :csv_file, CsvFileUploader

  # validates :file, uniqueness: { case_sensitive: true }
  # validates :csv_file, uniqueness: { case_sensitive: true }
  # validate :validate_file_type
  # validate :validate_csv_file_type
  # validates :agent, presence: true
  # validates :made_at, presence: true
  # validates :file, presence: true
  # validates_associated :segments

  # accepts_nested_attributes_for :segments

  def file_name
    return self.name if name
    return self.file_identifier if file_identifier
  end

  def online_flag
    # call_tag_names = {'angry': 'Angry call', 'upset': 'Upset call', 'stressed': 'Stress call', 'normal': 'Normal call'}
    # "#{tag_value}% - #{call_tag_names[tag_name]}"
    segments&.last&.online_lva
  end

  # def duration
  #   segments&.last&.end
  # end

  def file_status
    return 'rejected' unless file_statuses&.last&.status
    file_statuses.last.status
  end

  private

  # def validate_file_type

  # end

  # def validate_csv_file_type

  # end
end
