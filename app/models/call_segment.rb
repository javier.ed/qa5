class CallSegment < ApplicationRecord
  belongs_to :call

  validates :number, presence: true # 1
  validates :start, presence: true  # 4
  validates :end, presence: true    # 5
  # validates :cnl, presence: true  # 3
  validates :voice_energy, presence: true # 30
  # validates :lionet_analysis, presence: true
  # validates :offline_lva, presence: true # 6
  # validates :online_lva, presence: true  # 7
  # validates :comment, presence: true
  # validates :lionet_info, presence: true
  validates :energy, presence: true       # 12
  validates :content, presence: true      # 13
  validates :upset, presence: true        # 14
  validates :angry, presence: true        # 15
  validates :stress, presence: true       # 16
  # validates :uncertainty, presence: true
  # validates :excitement, presence: true
  validates :concentration_level, presence: true  # 19
  validates :emocog_ratio, presence: true         # 27
  validates :hesitation, presence: true           # 21
  # validates :brain_power, presence: true
  # validates :embarrassment, presence: true
  validates :intensive_thinking, presence: true   # 23
  validates :imagination_activity, presence: true # 24
  # validates :extreme_state, presence: true
  validates :saf, presence: true                  # 25
  # validates :atmosphere, presence: true
end
