class Profile < ApplicationRecord
  belongs_to :user

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :gender, presence: true
  validates :birthdate, presence: true
  validates :country_code, presence: true

  def country=(country)
    self.country_code = country
  end

  def country
    ISO3166::Country[country_code]
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
