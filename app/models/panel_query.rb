class PanelQuery < ApplicationRecord
  belongs_to :panel, inverse_of: :queries

  validates :query, presence: true
  validate :validate_query

  def run_query(args = {})
    args = args.with_indifferent_access
    args[:start_time] = "'#{Time.parse('2000-01-01')}'" unless args[:start_time]
    args[:end_time] = "'#{Time.current}'" unless args[:end_time]
    
    generated_query = query.gsub Regexp.compile(/:(\w+)/i) do
      %( #{replace_arg(Regexp.last_match(1), args)} )
    end
    result = ActiveRecord::Base.connection.exec_query(generated_query)

    return result.rows
  end

  private

  def replace_arg(key, args)
    return "#{args[key]}" if args[key]
    'NULL'
  end

  def validate_query
    begin
      run_query
    rescue
      errors.add(:query, 'Invalid query')
      false
    end
  end
end
