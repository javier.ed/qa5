class PanelsController < ApplicationController
  before_action :authenticate_user!, only: %[preview]
  before_action :set_args
  layout 'panel'

  def show
    @panel = Panel.find(params[:id])
  end

  def preview
    @panel = Panel.new(display_type: params[:display_type])

    params[:queries][:query].each_index do |i|
      @panel.queries.build(name: params[:queries][:name][i], query: params[:queries][:query][i])
    end

    case params[:location]
    when 'calls'
      @args[:call_id] = Call.first.id
    when 'agents'
      @args[:agent_id] = User.where(role: 'agent').first.id
    end

    render 'show'
  end

  private

  def set_args
    @args = {}
    @args[:call_id]= params[:call_id] if params[:call_id]
    @args[:agent_id]= params[:agent_id] if params[:agent_id]
    case params[:time_range]
    when 'last30days'
      @args[:start_time] = "'#{Time.current - 30.days}'"
      @args[:end_time] = "'#{Time.current}'"
    when 'last7days'
      @args[:start_time] = "'#{Time.current - 7.days}'"
      @args[:end_time] = "'#{Time.current}'"
    when 'last24hours'
      @args[:start_time] = "'#{Time.current - 1.days}'"
      @args[:end_time] = "'#{Time.current}'"
    else
      @args[:start_time] = "'#{Time.parse(params[:start_time])}'" if params[:start_time]
      @args[:end_time] = "'#{Time.parse(params[:end_time])}'" if params[:end_time]
    end
  end
end
