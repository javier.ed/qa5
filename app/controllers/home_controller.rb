class HomeController < ApplicationController
  before_action :authenticate_user!, only: %i[download]
  before_action :set_panels, only: %i[download]
  layout 'panel', only: %i[panel]

  def index; end

  def confirm_share
    ShareMailer.share(params[:share][:email], generate_pdf.render).deliver_later
    render json: { message: 'Report shared successfully' }, status: :created
  end

  def download
    pdf = generate_pdf
    send_data pdf.render, filename: "dashboard.pdf", type: 'application/pdf', disposition: 'inline'
  end

  def panel
    @panel = Panel.find(params[:id])
    @title = @panel.name
    @args = {}
    case params[:time_range]
    when 'last30days'
      @args[:start_time] = "'#{Time.current - 30.days}'"
      @args[:end_time] = "'#{Time.current}'"
    when 'last7days'
      @args[:start_time] = "'#{Time.current - 7.days}'"
      @args[:end_time] = "'#{Time.current}'"
    when 'last24hours'
      @args[:start_time] = "'#{Time.current - 1.days}'"
      @args[:end_time] = "'#{Time.current}'"
    else
      @args[:start_time] = "'#{Time.parse(params[:start_time])}'" if params[:start_time]
      @args[:end_time] = "'#{Time.parse(params[:end_time])}'" if params[:end_time]
    end
  end

  private

  def generate_pdf
    pdf = Prawn::Document.new
    pdf.text 'Dashboard'
    pdf.text "From #{@args[:start_time]} to #{@args[:end_time]}"
    options = Selenium::WebDriver::Firefox::Options.new binary: Settings.selenium[:binary], args: ['-headless']
    client = Selenium::WebDriver::Remote::Http::Default.new read_timeout: 120
    webdriver = Selenium::WebDriver.for :firefox, options: options, http_client: client
    pair = false
    y = 680
    @user_panels.each do |user_panel|
      x = pair ? 270 : 0
      pdf.bounding_box([x, y], width: 250, height: 200) do
        pdf.text_box user_panel.panel.name, at: [5, 195]
        pdf.move_down 20
        webdriver.navigate.to "#{Settings.protocol}://#{Settings.host}:#{Settings.port.to_s}/panels/#{user_panel.panel.id}?time_range=#{params[:time_range]}"
        webdriver.manage.window.resize_to(500, 400)
        webdriver.save_screenshot("tmp/panel_#{user_panel.id}_capture.png")
        pdf.image "tmp/panel_#{user_panel.id}_capture.png", scale: 0.5

        pdf.transparent(0.5) { pdf.stroke_bounds }
      end
      y -= pair ? 220 : 0
      pair = pair ? false : true
    end
    webdriver.quit
    return pdf
  end

  def set_panels
    @args = { start_time: "'#{Time.parse('2000-01-01')}'", end_time: "'#{Time.current}'" }
    case params[:time_range]
    when 'last30days'
      @args[:start_time] = "'#{Time.current - 30.days}'"
    when 'last7days'
      @args[:start_time] = "'#{Time.current - 7.days}'"
    when 'last24hours'
      @args[:start_time] = "'#{Time.current - 1.days}'"
    else
      @args[:start_time] = "'#{Time.parse(params[:start_time])}'" if params[:start_time]
      @args[:end_time] = "'#{Time.parse(params[:end_time])}'" if params[:end_time]
    end
    @user_panels = current_user.panels.joins(:panel).where('panels.location' => 'home').order('position DESC')
    @panels = Panel.where(location: 'home')
  end
end
