class CallsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!, only: %i[create download]
  before_action :set_call, only: %i[download]
  before_action :set_panels, only: %i[download]
  layout 'panel', only: %i[panel]

  def upload
    if params[:file].content_type == 'text/csv'
      csv_filename = params[:file].original_filename.downcase.gsub(/[()\ ]/, '_')
      filename = File.basename(csv_filename, File.extname(csv_filename))
      call = current_user.calls.where('LOWER(csv_file) = ? OR LOWER(file) = ?', csv_filename, filename).first
      call = current_user.calls.build unless call
      call.csv_file = params[:file]
      if call.save
        LvaJob.perform_later call
        render json: { success: true, message: 'File uploaded successfully', call: { id: call.id } }, status: :created
      else
        render json: { success: false, message: 'Failed to upload file', errors: call.errors }, status: :unprocessable_entity
      end
    elsif params[:file].content_type.match(/^audio\//)
      filename = params[:file].original_filename.downcase.gsub(/[()\ ]/, '_')
      csv_filename = "#{filename}.csv"
      call = current_user.calls.where('LOWER(csv_file) = ? OR LOWER(file) = ?', csv_filename, filename).first
      call = current_user.calls.build unless call
      call.file = params[:file]
      if call.save
        LvaJob.perform_later call unless call.csv_file
        render json: { success: true, message: 'File uploaded successfully', call: { id: call.id } }, status: :created
      else
        render json: { success: false, message: 'Failed to upload file', errors: call.errors }, status: :unprocessable_entity
      end
    end
  end

  def create
    call = current_user.calls.build(file: params[:file])
    if call.save
      LvaJob.perform_later call
      render json: { success: true, message: 'File uploaded successfully', call: { id: call.id } }, status: :created
    else
      render json: { success: false, message: 'Failed to upload file', errors: call.errors }, status: :unprocessable_entity
    end
  end

  def upload_report
    report = current_user.third_party_reports.build(file: params[:file])
    if report.save
      render json: { success: true, message: 'File uploaded successfully', report: { id: report.id } }, status: :created
    else
      render json: { success: false, message: 'Failed to upload file', errors: report.errors }, status: :unprocessable_entity
    end
  end

  def download
    pdf = generate_pdf
    send_data pdf.render, filename: "dashboard.pdf", type: 'application/pdf', disposition: 'inline'
  end

  def panel
    @panel = Panel.find(params[:id])
    @title = @panel.name
    @args = { call_id: params[:call_id] }
    render 'home/panel'
  end

  private

  def generate_pdf
    pdf = Prawn::Document.new
    pdf.text 'Call'
    pdf.text "File name: #{@call.file_name}"
    pdf.text "Made at: #{@call.made_at}"
    options = Selenium::WebDriver::Firefox::Options.new binary: Settings.selenium[:binary], args: ['-headless']
    client = Selenium::WebDriver::Remote::Http::Default.new read_timeout: 120
    webdriver = Selenium::WebDriver.for :firefox, options: options, http_client: client
    pair = false
    y = 660
    @user_panels.each do |user_panel|
      x = pair ? 270 : 0
      pdf.bounding_box([x, y], width: 250, height: 200) do
        pdf.text_box user_panel.panel.name, at: [5, 195]
        pdf.move_down 20
        webdriver.navigate.to "#{Settings.protocol}://#{Settings.host}:#{Settings.port.to_s}/panels/#{user_panel.panel.id}?call_id=#{@call.id}"
        webdriver.manage.window.resize_to(500, 400)
        webdriver.save_screenshot("tmp/panel_#{user_panel.id}_capture.png")
        pdf.image "tmp/panel_#{user_panel.id}_capture.png", scale: 0.5
        pdf.transparent(0.5) { pdf.stroke_bounds }
      end
      y -= pair ? 220 : 0
      pair = pair ? false : true
    end
    webdriver.quit
    return pdf
  end

  def set_call
    @call = Call.find(params[:id])
  end

  def set_panels
    @user_panels = current_user.panels.joins(:panel).where('panels.location' => 'calls')
    @panels = Panel.where(location: 'calls')
  end
end
