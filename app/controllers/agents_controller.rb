class AgentsController < ApplicationController
  before_action :authenticate_user!

  def index
    respond_to do |format|
      format.html { @agents = User.page(params[:page]) }
      format.json do
        users = []
        User.all.each do |user|
          users.append(id: user.id, username: user.username, full_name: user.profile.full_name)
        end
        render json: users
      end
    end
  end
end
