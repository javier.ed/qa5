class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale

  protected

  def set_locale
    cookies[:locale] ||= if user_signed_in?
                           current_user.person.lang
                         elsif request.env['HTTP_ACCEPT_LANGUAGE']&.scan(/^[a-z]{2}/)&.first
                           request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
                         else
                           I18n.default_locale
                         end
    cookies[:locale] = params[:locale] if params[:locale]
    I18n.locale = cookies[:locale]
  end
end
