Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_for :users, skip: %i[registrations sessions passwords]

  post 'graphql', to: 'graphql#execute'

  post 'calls' => 'calls#create'
  post 'calls/upload' => 'calls#upload'

  get 'panel/:id' => 'home#panel'
  get 'calls/:call_id/panel/:id' => 'home#panel', as: :calls_panel

  get 'panels/preview' => 'panels#preview'
  get 'panels/:id' => 'panels#show'
    
  get 'download' => 'home#download'
  get 'calls/:id/download' => 'calls#download', as: :calls_download

  get '*path', to: 'home#index'

  root to: 'home#index'
end
