const { environment } = require('@rails/webpacker')
const vue =  require('./loaders/vue')
const gql =  require('./loaders/gql')
const webpack = require('webpack');

// const settings = new webpack.ProvidePlugin({
//   Settings: './settings',
// })

const jquery = new webpack.ProvidePlugin({
  jQuery: 'jquery/dist/jquery.min',
  $: 'jquery/dist/jquery.min'
})

// environment.plugins.append('settings', settings)
environment.plugins.append('jquery', jquery)
environment.loaders.append('vue', vue)
environment.loaders.append('gql', gql)
module.exports = environment
