module Settings
  def self.recursive_merge(old_hash, new_hash)
    old_hash.merge(new_hash) do |_key, old_var, new_var|
      if old_var.is_a? Hash
        recursive_merge old_var, new_var.to_h
      else
        new_var
      end
    end
  end

  private_class_method :recursive_merge

  SOURCE = recursive_merge({
    'default' => {
      'host' => 'localhost',
      'port' => 3000,
      'protocol' => 'http',
      'name' => 'QA5',
      'description' => 'QA5',
      'tags' => %w[auth],
      'contact_mail' => nil,
      'create_user' => {
        'active' => true,
        'username_blacklist' => %w[admin administrator administrador auth cancel qa5 edit password
                                   profile sign_in sign_out sign_up sysadmin] },
      'mail' => {
        'enable' => false,
        'sender_address' => 'no-reply@localhost',
        'method' => 'sendmail',
        'location' => '/usr/sbin/sendmail',
        'exim_fix' => false,
        'host' => 'localhost',
        'post' => 587,
        'authentication' => 'plain',
        'username' => nil,
        'password' => nil,
        'starttls_auto' => true,
        'domain' => nil,
        'openssl_verify_mode' => nil,
        'ca_file' => nil },
      'lva' => { 'api_key' => nil },
      'selenium' => { 'browser' => 'firefox', 'binary' => '/usr/bin/firefox' },
      'sentry' => { 'dsn': nil } },
    Rails.env => {} }, YAML.load_file(Rails.root.join('config', 'settings.yml')) || {})

  SETTINGS = recursive_merge(SOURCE['default'], SOURCE[Rails.env]).with_indifferent_access

  private_constant :SOURCE, :SETTINGS

  def self.host
    SETTINGS[:host]
  end

  def self.port
    SETTINGS[:port].to_i
  end

  def self.protocol
    SETTINGS[:protocol]
  end

  def self.name
    SETTINGS[:name]
  end

  def self.description
    SETTINGS[:description]
  end

  def self.tags
    SETTINGS[:tags]
  end

  def self.contact_mail
    SETTINGS[:contact_mail]
  end

  def self.create_user
    SETTINGS[:create_user]
  end

  def self.mail
    SETTINGS[:mail]
  end

  def self.lva
    SETTINGS[:lva]
  end

  def self.selenium
    SETTINGS[:selenium]
  end

  def self.sentry
    SETTINGS[:sentry]
  end
end
