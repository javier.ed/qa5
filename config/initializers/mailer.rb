Rails.application.configure do
  config.action_mailer.perform_deliveries = Settings.mail[:enable]

  unless Rails.env.test? || !Settings.mail[:enable]
    if Settings.mail[:method] == 'sendmail'
      config.action_mailer.delivery_method = :sendmail
      sendmail_settings = { location: Settings.mail[:location] }
      sendmail_settings[:arguments] = "-i" if Settings.mail[:exim_fix]
      config.action_mailer.sendmail_settings = sendmail_settings
    elsif Settings.mail[:method] == 'smtp'
      config.action_mailer.delivery_method = :smtp
      smtp_settings = { address: Settings.mail[:host], port: Settings.mail[:port].to_i,
                        domain: Settings.mail[:domain], openssl_verify_mode: Settings.mail[:openssl_verify_mode],
                        ca_file: Settings.mail[:ca_file], enable_starttls_auto: false }

      if Settings.mail[:authentication] != 'none'
        smtp_settings.merge!({ authentication: Settings.mail[:authentication].gsub('-', '_').to_sym,
                               user_name: Settings.mail[:username], password: Settings.mail[:password],
                               enable_starttls_auto: Settings.mail[:starttls_auto] })
      end

      config.action_mailer.smtp_settings = smtp_settings
    else
      $stderr.puts "WARNING: Mailer turned on with unknown method #{Settings.mail[:method]}. Mail won't work."
    end
  end
end
